import React, { Component } from "react";

import "./App.css";
import Routers from "./Routers";
import "../src/styles/style.css";


class App extends Component {
    constructor(props) {
        super(props);
        this.loginHandler = this.loginHandler.bind(this);
    }

    state = {
        loggedIn: false,
    };

    loginHandler = () => {
        this.setState((prevState) => ({
            loggedIn: !prevState.loggedIn,
        }));
    };

    render() {
        return ( <
            Routers loggedIn = { this.state.loggedIn }
            loginHandler = { this.loginHandler }
            />
        );
    }
}

export default App;