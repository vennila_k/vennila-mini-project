import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Route } from "react-router-dom";
import ApplicationHomePage from "./components/ApplicationHomePage";
import AboutPage from "./components/AboutPage";
import HeaderNavLinkPage from "./components/HeaderNavLinkPage";
import HomePage from "./components/HomePage";
import LoginPage from "./components/LoginPage";
import { BloodDonorsPage } from "./components/BloodDonorsPage";

export default class Routers extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <HeaderNavLinkPage loggedIn={this.props.loggedIn} />
          <Route
            exact
            path="/"
            render={() => {
              return <HomePage />;
            }}
          />
          <Route
            exact
            strict
            path="/about"
            render={() => {
              return <AboutPage />;
            }}
          />
          <Route
            exact
            path="/login"
            render={() => {
              return <LoginPage loggedIn={this.props.loggedIn} loginHandler={this.props.loginHandler} />;
            }}
          />
          <Route
            exact
            path="/blood-donors"
            render={() => {
              return <BloodDonorsPage />;
            }}
          />
          <Route
            exact
            strict
            path="/home/:name"
            render={({ match }) => {   
                return (
                  <ApplicationHomePage
                    loginHandler={this.props.loginHandler}
                    loggedIn={this.props.loggedIn}
                    name={match.params.name}
                  />
                );
              }
            }
          />
        </div>
      </Router>
    );
  }
}
