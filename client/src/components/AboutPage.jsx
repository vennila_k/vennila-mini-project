import React from 'react';

export default class AboutPage extends React.Component {
  render() {
    return (
      <div>
        <h2>About Us</h2>
        <br />
        <div className="centeredDiv">
          <p>
              Blood Donar Management Mini Project
          </p>
          <br />
          <p>
          The main goal of the Blood Donor Management System project is to monitor Blood cells, Blood stock, Donor List, Last Donated List. It manages all the Blood Cells, Donor, Blood stock data. The project's aim is to develop an application system to minimize the manual work for Blood Bank, Donor, Blood Group management. It monitors all of the Blood Group information, Blood cells, Blood supply and Donor list.</p>
        </div>
      </div>
    );
  }
}
