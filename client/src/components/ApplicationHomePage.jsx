import React from "react";
import { Redirect } from "react-router-dom";

export default class ApplicationHomePage extends React.Component {
  render() {
    if (this.props.loggedIn) {
      return (
        <div>
          <h1>Application Home Page (After Login)</h1>
          <h1>Hi {this.props.name}</h1>
          <input
            type="button"
            value={this.props.loggedIn ? "Logout" : "Login"}
            onClick={this.props.loginHandler}
          />
        </div>
      );
    } else {
      return <Redirect to="/login" />;
    }
  }
}

