import React from "react";
import axios from "axios";
import moment from 'moment';

export default class BloodDonorItem extends React.Component {
  state = {
    donor: {},
  };

  componentDidMount() {
    axios
      .get(`http://localhost:9000/api/donor/get/` + this.props.donor.id)
      .then((res) => {
        const donor = res.data;
        this.setState({ donor });
      });
  }

  render() {
    return <li>
      <div className='donorContainer'>
        <div className="donorContainerLeft">
          <b>{this.state.donor.name}&nbsp;<span className="bloodGroupLabel">(&nbsp;Blood Group:&nbsp;{this.state.donor.blood_group}&nbsp;)</span></b><br />
          <span>Last Donated On: {moment(this.state.donor.last_donated_date).format('MMMM D, YYYY')}</span><br />
          <span>Age:&nbsp;{this.state.donor.age}&nbsp;years</span>
        </div>
      </div>
    </li>;
  }
}
