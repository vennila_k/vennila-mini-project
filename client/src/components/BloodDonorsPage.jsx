import React from "react";
import axios from "axios";
import BloodDonorItem from "./BloodDonorItem";

export class BloodDonorsPage extends React.Component {

  state = {
    donors: [],
  };

  componentDidMount() {
    axios.get(`http://localhost:9000/api/donor/getall`).then((res) => {
      const donors = res.data;
      this.setState({ donors });
    });
  }

  render() {
    return (
      <ul className="bloodDonorList">
        {this.state.donors.map((donor) => (
          <BloodDonorItem key={donor.id} donor={donor} />
        ))}
      </ul>
    );
  }
}
