import React from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";

export default class LoginPage extends React.Component {
  state = {
    email: "",
    password: ""
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit = event => {
    event.preventDefault();
    axios.post(`http://localhost:9000/api/user/login`, {
      email: this.state.email,
      password: this.state.password
    })
      .then(res => {
        console.log(res);
        alert(res.data.message);
        if (res.status === 200) {
          this.props.loginHandler();
        }
      })
  }

  render() {
    if (this.props.loggedIn) {
      return <Redirect to='/home/Vennila' />
    } else {
      return (
        <div>
          <h2>Login Page</h2>
          <br />
          <div className="login">
            <form id="login" onSubmit={this.handleSubmit}>
              <input type="text" className="inputField" name="email" value={this.state.email}
                onChange={this.handleChange} placeholder="Email" />
              <br />
              <br />
              <input type="password" className="inputField" name="password" value={this.state.password}
                onChange={this.handleChange} placeholder="Password" />
              <br />
              <br />
              <input type="submit" name="log" id="log" value="Log In" />
              <br />
              <br />
            </form>
          </div>
        </div>
      );
    }
  }
}
