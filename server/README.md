Name: Blood Donors Management API

Used technology: 
1.	NodeJS (version 12.18.0)
2.	PostgreSQL (version 13)

Database name: blood
Table name: donor

API List:
1.	CREATE
2.	GET ALL
3.	GET BY ID
4.	UPDATE BY ID
5.	DELETE BY ID

CREATE:
http://localhost:9000/api/donor/create
Method:  POST
Sample Request Body
--------------------------------------
{
  "name": "Vennila",
  "date_of_birth": "14/09/1993",
  "email": "vennila@gmail.com",
  "contact_no": "2344678990",
  "alternate_contact_no": "1246864357",
  "blood_group": "O=",
  "last_donated_date": 06/10/2020,
  "stopped_donating": false
}
--------------------------------------


GET ALL
http://localhost:9000/api/donor/getall
Method: GET


GET BY ID
http://localhost:9000/api/donor/get/1
Method: GET

UPDATE BY ID
http://localhost:9000/api/donor/update/1
Method:  PUT
Sample Request Body
--------------------------------------
{
  "stopped_donating": true
}
--------------------------------------

DELETE BY ID
http://localhost:9000/api/donor/delete/1
Method:  DELETE

IS DONOR ALREADY EXISTS
http://localhost:9000/api/donor/exists
Method:  POST
Sample Request Body
--------------------------------------
{
  "email": "vennila@gmail.com"
}
--------------------------------------






