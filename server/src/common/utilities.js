var constants = require("./constants");
var jwt = require("jwt-simple");

const utilities = {
    generateJWT: function(user) {
        var dateObj = new Date();
        return jwt.encode({
                exp: dateObj.setDate(
                    dateObj.getDate() + constants.TOKEN_VALIDITY_SEVEN_DAYS
                ),
                user: user,
                createdAt: new Date().getTime(),
            },
            constants.SECRET_KEY
        );
    },
    decodeJWT: function(accessToken) {
        let payload = null;
        if (accessToken) {
            payload = jwt.decode(accessToken, constants.SECRET_KEY);
        }
        return payload;
    },
};

module.exports = utilities;