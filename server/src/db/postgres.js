const knex = require('knex');
//Reference: http://knexjs.org/


const psql_config = {
  client: 'pg',
  connection: {
    host: 'localhost',
    database: 'postgres',
    user: 'postgres',
    password: 'postgres',
    port: '5432'
  }
};

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var psql_client = knex(psql_config);

module.exports = psql_client;