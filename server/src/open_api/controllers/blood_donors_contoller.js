const bloodDonorsRepo = require('../../repositories/blood_donors_repo');
var constants = require("../../common/constants");

module.exports.createDonor = function(req, res) {
    res.setHeader('content-Type', 'application/json');
    bloodDonorsRepo.createDonor(req.body, function(error, result) {
        if (error) {
            res.status(constants.http_status.INTERNAL_SERVER_ERROR).json(error);
        }
        if (result) {
            res.json(result);
        }
    });
}

module.exports.updateDonor = function(req, res) {
    res.setHeader('content-Type', 'application/json');
    bloodDonorsRepo.updateDonor(req.params.id, req.body, function(error, result) {
        if (error) {
            res.status(constants.http_status.INTERNAL_SERVER_ERROR).json(error);
        }
        if (result) {
            res.status(constants.http_status.OK).json({ "message": result });
        }
    });
}

module.exports.getAllDonors = function(req, res) {
    res.setHeader('content-Type', 'application/json');
    bloodDonorsRepo.getAllDonors(function(error, result) {
        if (error) {
            res.status(constants.http_status.INTERNAL_SERVER_ERROR).json(error);
        }
        if (result) {
            const records = result.rows ? result.rows : [];
            res.json(records);
        }
    });
}

module.exports.getDonorById = function(req, res) {
    const params = req.params;
    res.setHeader('content-Type', 'application/json');
    bloodDonorsRepo.getDonorById(params.id, function(error, result) {
        if (error) {
            res.status(constants.http_status.INTERNAL_SERVER_ERROR).json(error);
        }
        if (result) {
            const records = result.rows ? result.rows[0] : [];
            res.json(records);
        }
    });
}

module.exports.deleteDonorById = function(req, res) {
    res.setHeader('content-Type', 'application/json');
    bloodDonorsRepo.deleteDonorById(req.params.id, function(error, result) {
        if (error) {
            res.status(constants.http_status.INTERNAL_SERVER_ERROR).json(error);
        }
        if (result) {
            res.status(constants.http_status.OK).json({ "message": result });
        }
    });
}
