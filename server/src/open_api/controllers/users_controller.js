const userRepo = require("../../repositories/users_repo");
var constants = require("../../common/constants");
var utilities = require("../../common/utilities");

module.exports.createUser = function(req, res) {
    res.setHeader("content-Type", "application/json");
    userRepo.createUser(req.body, function(error, result) {
        if (error) {
            res.status(constants.http_status.INTERNAL_SERVER_ERROR).json(error);
        }
        if (result) {
            res.json(result);
        }
    });
};

module.exports.updateUser = function(req, res) {
    res.setHeader("content-Type", "application/json");
    userRepo.updateUser(req.params.id, req.body, function(error, result) {
        if (error) {
            res.status(constants.http_status.INTERNAL_SERVER_ERROR).json(error);
        }
        if (result) {
            res.status(constants.http_status.OK).json({ message: result });
        }
    });
};

module.exports.getAllUsers = function(req, res) {
    res.setHeader("content-Type", "application/json");
    userRepo.getAllUsers(function(error, result) {
        if (error) {
            res.status(constants.http_status.INTERNAL_SERVER_ERROR).json(error);
        }
        if (result) {
            const records = result.rows ? result.rows : [];
            res.json(records);
        }
    });
};

module.exports.getUserById = function(req, res) {
    const params = req.params;
    res.setHeader("content-Type", "application/json");
    userRepo.getUserById(params.id, function(error, result) {
        if (error) {
            res.status(constants.http_status.INTERNAL_SERVER_ERROR).json(error);
        }
        if (result) {
            const records = result.rows ? result.rows[0] : [];
            res.json(records);
        }
    });
};

module.exports.deleteUserById = function(req, res) {
    res.setHeader("content-Type", "application/json");
    userRepo.deleteUserById(req.params.id, function(error, result) {
        if (error) {
            res.status(constants.http_status.INTERNAL_SERVER_ERROR).json(error);
        }
        if (result) {
            res.status(constants.http_status.OK).json({ message: result });
        }
    });
};

module.exports.login = function(req, res) {
    res.setHeader("content-Type", "application/json");
    var email = req.body.email || "";
    var password = req.body.password || "";
    if (email === "" || password === "") {
        let msg = "Failed to authenticate. email or password cannot be null.";
        res.status(constants.http_status.UNAUTHORIZED);
        res.json({
            status: constants.http_status.UNAUTHORIZED,
            message: msg,
            payload: null,
            user_id: constants.user.DEFAULT_SUPER_ADMIN_USER_ID,
        });
    } else {
        auth.validate(email, password, function(err, data) {
            if (err) {
                userRepo.findUserByEmail(email, function(users) {
                    if (!users || users.rowCount <= 0) {
                        let msg =
                            "Failed to authenticate. Please check your email address and password.";
                        console.log("ERROR: User name does not exist in the system!!!");
                        const content = {
                            status: constants.http_status.UNAUTHORIZED,
                            message: "msg",
                            payload: null,
                        };
                        res.status(constants.http_status.UNAUTHORIZED).json(content);
                    } else {
                        console.log("users ==> " + JSON.stringify(users));
                        let msg =
                            "Failed to authenticate. Please check your email address and password.";
                        console.log(msg);
                        const content = {
                            status: constants.http_status.UNAUTHORIZED,
                            message: msg,
                            payload: null,
                        };
                        res.status(constants.http_status.UNAUTHORIZED).json(content);
                    }
                });
            } else {
                if (data) {
                    const jwt = utilities.generateJWT(data);
                    const payload = utilities.decodeJWT(jwt);
                    payload.accessToken = jwt;
                    res.status(constants.http_status.OK).json({
                        status: constants.http_status.OK,
                        message: "User successfully logged In",
                        payload: payload,
                    });
                }
            }
        });
    }
};

var auth = {
    validate: function(email, password, callback) {
        console.log(`Authenticating user by  email: ${email}`);
        userRepo.validateUser(email, password, function(error, result) {
            if (result && result.rowCount > 0) {
                console.log(`SUCCESS in authenticating user by email: ${email}`);
                const user = result.rows[0];
                callback(null, user);
            } else {
                const msg = `FAILED in authenticating user by email: ${email}`;
                console.log(msg);
                callback({ msg: msg }, null);
            }
        });
    },
};