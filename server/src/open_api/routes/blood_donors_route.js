var bloodDonorController = require('../controllers/blood_donors_contoller');

module.exports = function (router) {
    console.log('Server is ready now... try using api');
    router
        .route('/api/donor/create')
        .post(bloodDonorController.createDonor);
    router
        .route('/api/donor/update/:id')
        .put(bloodDonorController.updateDonor);
    router
        .route('/api/donor/delete/:id')
        .delete(bloodDonorController.deleteDonorById);
    router
        .route('/api/donor/getall')
        .get(bloodDonorController.getAllDonors);
    router
        .route('/api/donor/get/:id')
        .get(bloodDonorController.getDonorById);
    return router;
}