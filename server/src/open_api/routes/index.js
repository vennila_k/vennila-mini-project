const router = require('express').Router();

require('./blood_donors_route')(router);
require('./users_route')(router);

module.exports = router;