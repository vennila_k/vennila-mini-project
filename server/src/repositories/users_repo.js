const psql_client = require('../db/postgres');

module.exports.createUser = (user, callback) => {
    psql_client
        .insert(user)
        .into("users")
        .returning("*")
        .then(rows => callback(null, rows))
        .catch(error => callback(error, null));
}

module.exports.updateUser = (id, user, callback) => {
    psql_client
        .from('users')
        .where('id', '=', id)
        .update(user)
        .then(() => callback(null, `User with id->${id} updated successfully`))
        .catch(error => callback(error, null));
}

module.exports.getAllUsers = (callback) => {
    const query = "SELECT * FROM public.users";
    psql_client
        .raw(query)
        .then(rows => callback(null, rows))
        .catch(error => callback(error, null));
}

module.exports.getUserById = (userId, callback) => {
    const query = `SELECT * FROM public.users WHERE id=${userId}`;
    psql_client
        .raw(query)
        .then(rows => callback(null, rows))
        .catch(error => callback(error, null));
}

module.exports.deleteUserById = (id, callback) => {
    psql_client
        .from('users')
        .where({ id })
        .del()
        .then(() => callback(null, `User with id->${id} deleted successfully`))
        .catch(error => callback(error, null));
}

module.exports.findUserByEmail = (email, callback) => {
    const query = `SELECT * FROM public.users WHERE email='${email}'`;
    console.log(query);
    psql_client
        .raw(query)
        .then(data => callback(null, data))
        .catch(error => callback(error, null));
}

module.exports.validateUser = (email, password, callback) => {
    const query = `SELECT * FROM public.users WHERE email='${email}' and password='${password}'`;
    console.log(query);
    psql_client
        .raw(query)
        .then(data => callback(null, data))
        .catch(error => callback(error, null));
}